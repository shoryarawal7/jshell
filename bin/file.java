import java.util.Scanner;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
public class file{
	public static void main(String[] args) throws IOException{
		String mode = args[0];
		if(mode.equals("--info") || mode.equals("-i")){
			ReadFile("information.jinfo");
		}else{
			String fileName = args[1];
			if(mode.equals("--create") || mode.equals("-c")){
				CreateFile(fileName);
			} else if(mode.equals("--read") || mode.equals("-r")){
				ReadFile(fileName);
			} else if(mode.equals("--write") || mode.equals("-w")){
				ReadFile(fileName);
				WriteFile(fileName);
			} else if(mode.equals("--flush") || mode.equals("-f")){
				Flush(fileName);	
			} else{
				System.err.println("incorrect mode.");
			}
		}
	}
	
	static void ReadFile(String filename){
		String line;
		try{
			FileReader inStream = new FileReader(filename);
			BufferedReader reader = new BufferedReader(inStream);
			while((line = reader.readLine()) != null){
				System.out.println(line);
			}
		} catch(IOException e){
			System.err.println("Error: " + e);
		}
	}
	
	static void WriteFile(String filename){
		Scanner sc = new Scanner(System.in);
		String line;
		try{
			FileWriter outStream = new FileWriter(filename, true);
			BufferedWriter writer = new BufferedWriter(outStream);
			while(true){
				System.out.print(">");
				line = sc.nextLine();
				if(line.equals("EOP")){
					break;
				} else{
					writer.write(line);
					writer.newLine();
				}
			}
			writer.close();
			outStream.close();			
		} catch(IOException e){
			System.err.println("Error: " + e);
		}
	}

	static void CreateFile(String filename){
		try{
			File file = new File(filename);
			if(file.createNewFile()){
				System.out.println(file.getName() + " created.");
			}else{
				System.out.println("File already exists.");
			}
		}catch (IOException e){
			System.err.println("Error: " + e);
		}
	}

	static void Flush(String filename){
		try{
			FileWriter outStream = new FileWriter(filename);
			outStream.write("");
			outStream.close();
			System.out.println("File flushed successdully");
		} catch (IOException e){
			System.err.println("Error: " + e);
		}
	}
}
