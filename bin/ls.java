import java.io.File;
public class ls{
	public static void main(String[] args){
		String path = System.getProperty("user.dir");
		File fObj = new File(path);
		if(fObj.exists() && fObj.isDirectory()){
			File a[] = fObj.listFiles();
			printFileName(a, 0, 0);
		}
	}

	static void printFileName(File[] a, int i, int lvl){
		if(i == a.length){
			return;
		}
		if(a[i].isFile()){
			System.out.println(i + " " + a[i].getName());
		}
		printFileName(a, i+1, lvl);
	}
	
}
