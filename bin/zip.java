import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import java.util.Scanner;
public class zip{
	public static void main(String[] args) throws IOException{
		Scanner sc = new Scanner(System.in);
		System.out.print("zip or unzip: ");
		String bool1 = sc.nextLine();
		if(bool1.equals("zip") || bool1.equals("z")){
			System.out.print("\nFile to compress: ");
			String Filename = sc.nextLine();
			zip(Filename);
		}else if(bool1.equals("unzip") || bool1.equals("u")){
			System.out.print("\nFile to Decompress: ");
			String inputfile = sc.nextLine();
			System.out.print("New Filename: ");
			String outputfile = sc.nextLine();
			unzip(inputfile, outputfile);
		}else{
			System.err.println("Invalid Choice");
		}
	}

	static void zip(String inputFile) throws IOException{
		FileInputStream inStream = new FileInputStream(inputFile);
		FileOutputStream outStream = new FileOutputStream(inputFile + ".zip");
		DeflaterOutputStream compresser = new DeflaterOutputStream(outStream);
		int data;
		while((data = inStream.read()) != -1){
			compresser.write(data);
		}
		inStream.close();
		compresser.close();
	}

	static void unzip(String inputFile, String outputFile) throws IOException{
		FileInputStream inStream = new FileInputStream(inputFile);
		FileOutputStream outStream = new FileOutputStream(outputFile);
		InflaterInputStream decompressor = new InflaterInputStream(inStream);
		int data;
		while((data = inStream.read()) != -1){
			outStream.write(data);
		}
		outStream.close();
		decompressor.close();
	}	
}
