import java.io.*;
import java.util.Scanner;
public class jshell{
	public static void main(String[] args) throws IOException{
		Scanner sc = new Scanner(System.in);
		while(true){
			System.out.print("\n" + System.getProperty("user.dir") + " _$ ");
			String cmd = sc.next();
			if(cmd.equals("exit")){
				break;
			} else if(cmd.equals("info")){
				information();
			} else{
				File f = new File("bin/" + cmd + ".java");
				if(f.exists() && !f.isDirectory()){
					engine(cmd);
				}else{
					System.err.println("command does not exits");
				}
			}
		}
	}
	static void engine(String program) throws IOException{
		Process p = null;
		BufferedReader inStream = null;
		try{
			p = Runtime.getRuntime().exec("java bin/" + program + ".java");
		} catch(IOException e){
			System.err.println("Error in the engine");
			e.printStackTrace();
		}

		try{
			inStream = new BufferedReader(new InputStreamReader( p.getInputStream() ));
			System.out.println(inStream.readLine());
			inStream.close();
		} catch(IOException e){
			System.err.println("Error on instream.readLines()");
			e.printStackTrace();
		}
	}
	
	static void information() throws IOException{
		String filename = "doc/information.jinfo";
		String line = null;
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			while((line = bufferedReader.readLine()) != null){
				System.out.println(line);
			}
			bufferedReader.close();
		} catch(IOException e){
			System.err.println("Information.jinfo does not exist");
		}
	}
}
